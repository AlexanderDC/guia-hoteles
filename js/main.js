(function() {

    'use strict'


    var preloaded = document.getElementById("preloaded");
    preloaded.style.display = "flex";
    document.addEventListener("DOMContentLoaded", function() {

        preloaded.style.display = "none";






    });



})();


$(function() {

    $("[data-toggle = 'tooltip']").tooltip();
    $("[data-toggle = 'popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#modal-notificacion').on('show.bs.modal', function(e) {
        console.log('se abrio el modal de notificacion');
        $("#notificacion").removeClass("btn-outline-warning");
        $("#notificacion").addClass("btn-warning");
        $("#notificacion").prop("disabled", true);
    });
    $("#modal-notificacion").on("shown.bs.modal", function(e) {
        console.log("se termino de abrior el modal de notificacion");
    });
    $("#modal-notificacion").on("hide.bs.modal", function(e) {
        console.log("se esta cerrando el modal de notificacion");
    });
    $("#modal-notificacion").on("hidden.bs.modal", function(e) {
        console.log("se cerro el modal de notificacion");
        $("#notificacion").removeClass("btn-warning");
        $("#notificacion").addClass("btn-outline-warning");
        $("#notificacion").prop("disabled", false);
    });


});